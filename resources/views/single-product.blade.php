@extends('layouts.app')

@section('content')

 <!-- Page Content -->
    <!-- Single Starts Here -->
    <div class="single-product">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <div class="line-dec"></div>
              <h1>Single Product</h1>
            </div>
          </div>
          <div class="col-md-6">
            <div class="product-slider">
              <div id="slider" class="flexslider">
                <ul class="slides">
                  <li>
                    <img src="{{asset($product->path)}}" />
                  </li>
                  
                  <!-- items mirrored twice, total of 12 -->
                </ul>
              </div>
              <div id="carousel" class="flexslider">
             
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="right-content">
              {{-- <h2>a{{$product->discount}}</h2>
              <h2>b{{$product->price }}</h2>
              <h2>c{{$product->price * $product->discount}}</h2> --}}
              
              <h4>{{$product->name}}</h4>
              <h6>{{$product->price *  auth()->user()->discount}}s.p</h6>
              <p>{{$product->details}} </p>
              <span>{{$p=$product->q_large+$product->q_small+$product->q_medium}} left on stock</span>
              <form action="{{route('coupon.check')}}" method="get">
                @csrf
                <h5>Coupon</h5>
                <input type="text" name="coupon_name" id="coupon_name">
                <input type="hidden" name="user_id" value="{{auth()->user()->id}}" id="user_id">
                <input type="submit" class="btn">
              </form>

              <form action="{{route('order.store')}}" method="post">
                @csrf
                <input name="product_id" type="hidden" value="{{$product->id}}" class="address-text" id="product_id" >
                <input name="price" type="hidden" value="{{$product->price=$product->price * auth()->user()->discount}}" class="address-text" id="price" >
                <label for="address">Address:</label>
                <input name="address" type="string" class="address-text" id="address" >
                <br>
                <label for="quantity">Quantity:</label>
                <input name="quantity" type="rang" class="quantity-text" id="quantity" 
                onfocus="if(this.value == '1') { this.value = ''; }" 
                onBlur="if(this.value == '') { this.value = '1';}"
                value="1" min="1" max="">
                <label for="size">Size:</label>
                <select name="size" id="size">
                  @if ($product->q_large >= 1 )
                  <option value="large" >Large</option>
                  @endif
                  
                  @if ($product->q_small >= 1)
                  <option value="small">Small</option>
                  @endif
                  
                  @if ($product->q_medium >= 1)
                  <option value="medium">Medium</option>
                  @endif
                  
                </select>
                @if ($product->price != $product->price * auth()->user()->discount)
                <input name="coupon" type="hidden" value="yes" id="coupon">      
                @endif
                <br>
                @if ($product->price <= auth()->user()->money)
                <input type="submit" class="button" value="Order Now!">
                @else
                <input type="button" onclick="alert('charge money')" class="btn btn-danger" value="Order Now!">
                @endif
              </form>
              <div class="down-content">
                <div class="categories">
                  <h6>Category: <span><a href="#">{{$product->category->name}}</a></span></h6>
                </div>
                <div class="share">
                  <h6>Share: <span><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-linkedin"></i></a><a href="#"><i class="fa fa-twitter"></i></a></span></h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Single Page Ends Here -->


    <!-- Similar Starts Here -->
    {{-- <div class="featured-items">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <div class="line-dec"></div>
              <h1>You May Also Like</h1>
            </div>
          </div>
          <div class="col-md-12">
            <div class="owl-carousel owl-theme">
              <a href="single-product.html">
                <div class="featured-item">
                  <img src="assets/images/item-01.jpg" alt="Item 1">
                  <h4>Proin vel ligula</h4>
                  <h6>$15.00</h6>
                </div>
              </a>
             
            </div>
          </div>
        </div>
      </div>
    </div> --}}
    <!-- Similar Ends Here -->


    <!-- Subscribe Form Starts Here -->
    <div class="subscribe-form">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <div class="line-dec"></div>
              <h1>Subscribe on PIXIE now!</h1>
            </div>
          </div>
          <div class="col-md-8 offset-md-2">
            <div class="main-content">
              <p>Godard four dollar toast prism, authentic heirloom raw denim messenger bag gochujang put a bird on it celiac readymade vice.</p>
              <div class="container">
                <form id="subscribe" action="" method="get">
                  <div class="row">
                    <div class="col-md-7">
                      <fieldset>
                        <input name="email" type="text" class="form-control" id="email" 
                        onfocus="if(this.value == 'Your Email...') { this.value = ''; }" 
                    	onBlur="if(this.value == '') { this.value = 'Your Email...';}"
                    	value="Your Email..." required="">
                      </fieldset>
                    </div>
                    <div class="col-md-5">
                      <fieldset>
                        <button type="submit" id="form-submit" class="button">Subscribe Now!</button>
                      </fieldset>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Subscribe Form Ends Here -->

@endsection