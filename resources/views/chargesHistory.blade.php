@extends('layouts.app')

@section('content')

<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <a class="navbar-brand" href="#"><img src=" " alt=""></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item ">
            <a class="nav-link" href="{{route('order.index')}}">Orders
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="{{ route('charge.create')}}">Charge
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="{{ route('charge.index')}}">Charges History
            </a>
          </li>
          </ul>
      </div>
    </div>
  </nav>
  
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"> </h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
            <tr>
                <th>Charge</th>
                <th>Value</th>
                <th>Code</th>
                <th>Status</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
              <th>Charge</th>
              <th>Value</th>
              <th>Code</th>
              <th>Status</th>
            </tr>
          </tfoot>
          <tbody>
         @foreach ($charges as $charge )
           
         <tr>
                <td> <h3> #{{$charge->id}}  </h3> </td>
                <td> <h5> {{$charge->value}}  </h5></td>
                <td><h5> {{$charge->code}} </h5> </td>
                <td>  @if ($charge->status_id==0)
                  <h5 style="color: rgb(183, 191, 40)">  Pending </h5>
                @elseif ($charge->status_id==1)
                 <h5 style="color: rgba(16, 197, 25, 0.73)"> Accepted</h5>
                 @else
                 <h5 style="color: rgba(197, 19, 16, 0.73)"> Refused</h5>
                 @endif </td>  
                
            </tr>
            @endforeach
          </tbody>
          </table>
      </div>
    </div>
  </div>

@endsection