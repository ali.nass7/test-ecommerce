

@extends('layouts.app')
<link rel="stylesheet" href="{{asset('assets\css\app.css')}}">
@section('content')

<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
  <div class="container">
    <a class="navbar-brand" href="#"><img src=" " alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item ">
          <a class="nav-link" href="{{route('order.index')}}">Orders
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{ route('charge.create')}}">Charge
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{ route('charge.index')}}">Charges History
          </a>
        </li>
        </ul>
    </div>
  </div>
</nav>
<br>
<br>
<br>
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"> </h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
            <tr>
                <th>Order</th>
                <th>Product Name</th>
                <th>Address</th>
                <th>Size</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Date</th>
                <th>image</th>
               
                
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Order</th>
              <th>Product Name</th>
              <th>Address</th>
              <th>Size</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Date</th>
              <th>image</th>
          
            </tr>
          </tfoot>
          <tbody>
            @foreach ($orders as $order )
            <tr>
                <td> <h3> #{{$order->id}} </h3> </td>
                <td> <h5> {{$order->product->name}} </h5></td>
                <td> 
                  <h5>  {{$order->address}} </h5>
                </td>
                <td><h5> {{$order->size}} </h5> </td>
 
                <td> 
                  <h5> {{$order->quantity}} </h5>
                </td>
                <td> 
                  <h5> {{$order->price}} s.p </h5>
                </td>
                <td> 
                 <h5> {{$order->created_at}} </h5>
                </td>
                <td> 
                 <img src="{{$order->product->path}}" alt="" height="40px">
                </td>
                
            </tr>
           @endforeach
          </tbody>
          </table>
      </div>
    </div>
  </div>


@endsection

