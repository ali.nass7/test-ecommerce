@extends('layouts.adminlayout')

@section('content')

<style>
  /*the container must be positioned relative:*/
  .custom-selectg {
    position: relative;
    font-family: Arial;
  }
  
  .custom-selectg select {
    display: none; /*hide original SELECT element:*/
  }
  
  .select-selectedg {
    background-color: DodgerBlue;
  }
  
  /*style the arrow inside the select element:*/
  .select-selectedg:after {
    position: absolute;
    content: "";
    top: 14px;
    right: 10px;
    width: 0;
    height: 0;
    border: 6px solid transparent;
    border-color: #fff transparent transparent transparent;
  }
  
  /*point the arrow upwards when the select box is open (active):*/
  .select-selectedg.select-arrow-activeg:after {
    border-color: transparent transparent #fff transparent;
    top: 7px;
  }
  
  /*style the items (options), including the selected item:*/
  .select-itemsg div,.select-selectedg {
    color: #ffffff;
    padding: 8px 16px;
    border: 1px solid transparent;
    border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
    cursor: pointer;
    user-select: none;
  }
  
  /*style items (options):*/
  .select-itemsg {
    position: absolute;
    background-color: DodgerBlue;
    top: 100%;
    left: 0;
    right: 0;
    z-index: 99;
  }
  
  /*hide the items when the select box is closed:*/
  .select-hideg {
    display: none;
  }
  
  .select-itemsg div:hover, .same-as-selectedg {
    background-color: rgba(0, 0, 0, 0.1);
  }
  </style>
  
<style>
        .grid-container {
          display: grid;
          gap: 5px;
          background-color: #ffffff;
          padding: 10px;
        }
        
        .grid-item {
          background-color: hsla(222, 85%, 38%, 0.798);
          text-align: center;
          padding: 20px;
          font-size: 30px;
          color: #000;
        }
        
        .item1 {
          grid-column: 1 ;
          grid-row: 1;
        }
        
        .item2 {
          grid-column: 2;
          grid-row: 1;
        }
        .item3 {
          grid-column: 3;
          grid-row: 1;
        }
        .item4 {
          grid-column: 1;
          grid-row: 2 ;
        }
        
        .item5 {
          grid-column: 2 ;
          grid-row: 2;
        }
        .item6 {
          grid-column: 3 ;
          grid-row: 2;
        }
        /* .item1 {
          grid-column: 1 / span 2;
          grid-row: 1;
        }
        
        .item2 {
          grid-column: 3;
          grid-row: 1 / span 2;
        }
        
        .item5 {
          grid-column: 1 / span 3;
          grid-row: 3;
        } */
        </style>
        
        
        
        <div class="grid-container">
          <div class="grid-item item1"><h1>The Profit:{{$money}}</h1></div>
          <div class="grid-item item2"><h1>Quantity Sold :{{$quantity}}</h1></div>
          <div class="grid-item item3"><h1> Total Ordars: {{$totalOrdars}} </h1></div>  
          <div class="grid-item item4"><h1> Users Money : {{$usersMoney}} </h1></div>
          <div class="grid-item item5"><h1> Count Of Charge: {{$countOfCharge}} </h1></div>
          <div class="grid-item item6"><h1> Total Of Users {{$users}} </h1></div>
        </div>
        <div class="d-flex justify-content-center">
          <form  action="{{route('admin.weather.change')}}" method="POST">
            @csrf
            @method('PATCH')
            Set The Weather: 
            <div class="custom-selectg" style="width:200px;">
                <select name="weather" id="weather">
                  @foreach ($weathers as $weather)
                  <option value="{{$weather->id}}">{{$weather->name}}</option> 
                  @endforeach 
                </select>
            </div>
                <input class="btn btn-outline-secondary" type="submit" value="save">
              </form>
        
            </div>
            
            <script>
              var x, i, j, l, ll, selElmnt, a, b, c;
              /*look for any elements with the class "custom-select":*/
              x = document.getElementsByClassName("custom-selectg");
              l = x.length;
              for (i = 0; i < l; i++) {
                selElmnt = x[i].getElementsByTagName("select")[0];
                ll = selElmnt.length;
                /*for each element, create a new DIV that will act as the selected item:*/
                a = document.createElement("DIV");
                a.setAttribute("class", "select-selectedg");
                a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
                x[i].appendChild(a);
                /*for each element, create a new DIV that will contain the option list:*/
                b = document.createElement("DIV");
                b.setAttribute("class", "select-itemsg select-hideg");
                for (j = 1; j < ll; j++) {
                  /*for each option in the original select element,
                  create a new DIV that will act as an option item:*/
                  c = document.createElement("DIV");
                  c.innerHTML = selElmnt.options[j].innerHTML;
                  c.addEventListener("click", function(e) {
                      /*when an item is clicked, update the original select box,
                      and the selected item:*/
                      var y, i, k, s, h, sl, yl;
                      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                      sl = s.length;
                      h = this.parentNode.previousSibling;
                      for (i = 0; i < sl; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                          s.selectedIndex = i;
                          h.innerHTML = this.innerHTML;
                          y = this.parentNode.getElementsByClassName("same-as-selectedg");
                          yl = y.length;
                          for (k = 0; k < yl; k++) {
                            y[k].removeAttribute("class");
                          }
                          this.setAttribute("class", "same-as-selectedg");
                          break;
                        }
                      }
                      h.click();
                  });
                  b.appendChild(c);
                }
                x[i].appendChild(b);
                a.addEventListener("click", function(e) {
                    /*when the select box is clicked, close any other select boxes,
                    and open/close the current select box:*/
                    e.stopPropagation();
                    closeAllSelect(this);
                    this.nextSibling.classList.toggle("select-hideg");
                    this.classList.toggle("select-arrow-activeg");
                  });
              }
              function closeAllSelect(elmnt) {
                /*a function that will close all select boxes in the document,
                except the current select box:*/
                var x, y, i, xl, yl, arrNo = [];
                x = document.getElementsByClassName("select-itemsg");
                y = document.getElementsByClassName("select-selectedg");
                xl = x.length;
                yl = y.length;
                for (i = 0; i < yl; i++) {
                  if (elmnt == y[i]) {
                    arrNo.push(i)
                  } else {
                    y[i].classList.remove("select-arrow-activeg");
                  }
                }
                for (i = 0; i < xl; i++) {
                  if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hideg");
                  }
                }
              }
              /*if the user clicks anywhere outside the select box,
              then close all select boxes:*/
              document.addEventListener("click", closeAllSelect);
              </script>
              
@endsection