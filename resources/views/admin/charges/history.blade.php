@extends('layouts.adminlayout')

@section('content')

<h1>Pendding Charges</h1>
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
            <tr>
              <th>id user</th>
              <th>Name</th>
              <th>Money</th>
              <th>Value</th>
              <th>Code</th>
              <th>when charged</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
                <th>id user</th>
                <th>Name</th>
                <th>Money</th>
                <th>Value</th>
                <th>Code</th>
                <th>when charged</th>
              </tr>
            </tfoot>
          <tbody>
            @foreach ($charges as $charge )
            <tr>
                <td> {{$charge->user->id}}</td>
                <td> {{$charge->user->name}} </td>
                <td> {{$charge->user->money}} </td>
                <td> {{$charge->value}} </td>
                <td> {{$charge->code}} </td>
                <td> {{$charge->balance}} </td>
                
              </tr>
              @endforeach
            </tbody>
      </div>
    </div>
  </div>
@endsection
