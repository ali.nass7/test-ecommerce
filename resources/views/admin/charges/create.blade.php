@extends('layouts.adminlayout')

@section('content')
<h1>Insert a new Coupons</h1>
<form action="{{route('admin.charge.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" class="form-control" id="email" width="40%" >
    </div>
    <div class="form-group">
        <label for="value">value</label>
        <input type="number"  name="value" class="form-control" id="value" width="40%" >
    </div>
    
    
    <button type="submit" class="btn btn-primary">insert</button>
</form>
@endsection