@extends('layouts.adminlayout')

@section('content')

<h1>Pendding Charges</h1>
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
            <tr>
                <th>Name</th>
                <th>Money</th>
                <th>Value</th>
                <th>Code</th>
                <th>Accept</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Name</th>
                <th>Money</th>
                <th>Value</th>
                <th>Code</th>
                <th>Accept</th>
                <th>Delete</th>
            </tr>
          </tfoot>
          <tbody>
            @foreach ($charges as $charge )
            <tr>
                <td> {{$charge->user->name}} </td>
                <td> {{$charge->user->money}} </td>
                <td> {{$charge->value}} </td>
                <td> {{$charge->code}} </td>
                <td> 
                  <form method="POST" action="{{route('admin.charge.update',$charge->id)}}">
                    @csrf
                    @method('PATCH')
                    <button class="btn btn-danger">Accept</button>
                  </form>  
                </td>
                <td>
                  <form method="POST" action="{{route('admin.charge.destroy',$charge->id)}}">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Delete</button>
                  </form>  
                </td>
            </tr>
            @endforeach
          </tbody>
      </div>
    </div>
  </div>
@endsection
