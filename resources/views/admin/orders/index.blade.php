@extends('layouts.adminlayout')

@section('content')
<link rel="stylesheet" href="{{asset('assets\css\app.css')}}">
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"> </h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
            <tr>
                <th>Order</th>
                <th>User</th>
                <th>Product Name</th>
                <th>Code</th>
                <th>Size</th>
                <th>Quantity</th>
                <th>Coupon</th>
                <th>Price</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Order</th>
                <th>User</th>
                <th>Product Name</th>
                <th>Code</th>
                <th>Size</th>
                <th>Quantity</th>
                <th>Coupon</th>
                <th>Price</th>
              </tr>
          </tfoot>
          <tbody>
         @foreach ($orders as $order )
           
         <tr>
                <td> <h3> #{{$order->id}}  </h3> </td>
                <td> <h5> {{$order->user->name}}  </h5></td>
                <td> <h5> {{$order->product->name}}  </h5></td>
                <td><h5> {{$order->address}} </h5> </td>
                <td>  <h5> {{$order->size}} </h5> </td>  
                <td>  <h5> {{$order->quantity}} </h5> </td>  
                <td>  <h5> {{$order->coupon}} </h5> </td>  
                <td>  <h5> {{$order->price}} </h5> </td>  
                
            </tr>
            @endforeach
          </tbody>
          </table>
      </div>
    </div>
  </div>
@endsection