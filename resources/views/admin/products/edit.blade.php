@extends('layouts.adminlayout')

@section('content')

<h1>Edit a new Product</h1>
<form  method="post" action="{{route('admin.product.update',$product->id)}}" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" id="name" width="40%" value="{{$product->name}}" >
    </div>
    <div class="form-group">
        <img src="{{$product->path}}" alt="" height="100px">
        <label for="image">Image</label>
        <input type="file" name="path" class="form-control-file" id="path">
    </div>
    <div class="form-group">
        <label for="details">details</label>
        <textarea name="details" class="form-control" id="details" cols="30" rows="3"> {{"$product->details"}} </textarea>
    </div>
    <div class="form-group">
        <label for="size">sizes</label>
        <br>
        <label for="q_large">large</label>
        <input type="number" name="q_large" class="" id="q_large" value="{{$product->q_large}}">
        <label for="q_medium">meduim</label>
        <input type="number" name="q_medium" class="" id="q_medium" value="{{$product->q_medium}}" >
        <label for="q_small">small</label>
        <input type="number" name="q_small" class="" id="q_small" value="{{$product->q_small}}">
    </div>
    <div class="form-group">
        <label for="price">price</label>
        <input type="number" name="price" class="form-control-select" id="price" value="{{$product->price}}">
    </div>
    <div class="form-group">
        <label for="hide">hide</label>
        <select name="hide" id="hide">
            @if ($product->hide)
                <option value="1">hide</option>
                <option value="0">view</option>
            @else
            <option value="0">view</option>
            <option value="1">hide</option>
            @endif
        </select>
    </div>
    <div class="form-group">
        <label for="category_id">category</label>
        <select name="category_id" class="form-control-select" id="category_id" value="{{$product->category_id}}">
            <option value="{{$product->category_id}}" hidden>{{$product->category->name}}</option>
            <option value="1">man</option>
            <option value="2">woman</option>
        </select>
        {{-- <input type="number" name="category_id" class="form-control-select" id="category_id" > --}}
        
    </div>
    <div class="form-group">
        {{-- <label for="weather_id">Weather</label>
        <select name="weather_id" class="form-control-select" id="weather_id" value="{{$product->weather_id}}" >
            <option value="{{$product->weather_id}}" hidden > {{$product->weather->name}} </option>
            <option value="1">Summer</option>
            <option value="2">Winter</option>
            <option value="3">Autumn</option>
            <option value="4">Spring</option>
        </select> --}}
        <input type="checkbox" name="weather_id[]" id="weather_id" value="1" {{ $product->weathers->contains(1) ? 'checked' : '' }}>summer
<input type="checkbox" name="weather_id[]" id="weather_id" value="2" {{ $product->weathers->contains(2) ? 'checked' : '' }}>winter
<input type="checkbox" name="weather_id[]" id="weather_id" value="3" {{ $product->weathers->contains(3) ? 'checked' : '' }}>autumn
<input type="checkbox" name="weather_id[]" id="weather_id" value="4" {{ $product->weathers->contains(4) ? 'checked' : '' }}>spring
    </div>
    <button type="submit" class="btn btn-primary">update</button>
</form>

@endsection