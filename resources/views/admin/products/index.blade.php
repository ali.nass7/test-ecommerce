@extends('layouts.adminlayout')

@section('content')

<h1>All Product</h1>
@if (Session::has('message'))
<div class="alert alert-danger">
    {{Session::get('message')}}
</div>
@elseif (Session::has('message-product-created'))
<div class="alert alert-success">
  {{Session::get('message-product-created')}}
</div>
  @endif
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
            <tr>
                <th>name</th>
                <th>details</th>
                <th>Image</th>
                <th>large</th>
                <th>meduim</th>
                <th>small</th>
                <th>price</th>
                <th>category</th>
                <th>weather</th>
                <th>delete</th>
                <th>edit</th>
              </tr>
            </thead>
          <tfoot>
            <tr>
              <th>name</th>
              <th>details</th>
              <th>Image</th>
              <th>large</th>
              <th>meduim</th>
              <th>small</th>
              <th>price</th>
              <th>category</th>
              <th>weather</th>
              <th>delete</th>
              <th>edit</th>
            </tr>
          </tfoot>
          <tbody>
            @foreach ($products as $product )
            <tr>
                <td> {{$product->name}} </td>
                <td>  {{$product->details}} </td>
                <td> 
                   <div>
                    <img src="{{$product->path}}" alt="" height="40px">     
                  </div>
                </td>
                <td> {{$product->q_large}} </td>
                <td> {{$product->q_medium}} </td>
                <td> {{$product->q_small}} </td>
                <td> {{$product->price}} </td>
                <td> {{$product->category->name}} </td>
                {{-- <td> {{$product->weather->name}} </td> --}}
                <td>
                  @foreach ($product->weathers as $weather )
                    {{$weather->name}} ,
                  @endforeach
                </td>
                
                <td> 
                  <form method="POST" action="{{route('admin.product.destroy',$product->id)}}">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Delete</button>
                  </form>  
                </td>
                <td> 
                  <button class="btn btn-primary">
                    Edit<a href="{{route('admin.product.edit',$product->id)}}">t</a>
                  </button>
                </td>
            </tr>
            @endforeach
          </tbody>
          </table>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
      <!-- Page level plugins -->
  <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
    {{-- <script src="{{asset('js/datatabels')}}"></script> --}}
@endsection