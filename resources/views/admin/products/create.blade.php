@extends('layouts.adminlayout')

@section('content')

<h1>Insert a new Product</h1>
<form action="{{route('admin.product.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" id="name" width="40%" >
    </div>
    <div class="form-group">
        <label for="image">Image</label>
        <input type="file" name="path" class="form-control-file" id="path">
    </div>
    <div class="form-group">
        <label for="details">details</label>
        <textarea name="details" class="form-control" id="details" cols="30" rows="3"></textarea>
    </div>
    <div class="form-group">
        <label for="size">sizes</label>
        <br>
        <label for="q_large">large</label>
        <input type="number" name="q_large" class="" id="q_large" >
        <label for="q_medium">meduim</label>
        <input type="number" name="q_medium" class="" id="q_medium" >
        <label for="q_small">small</label>
        <input type="number" name="q_small" class="" id="q_small" >
    </div>
    <div class="form-group">
        <label for="purchase">purchase</label>
        <input type="number" name="purchase" class="form-control-select" id="price" >
    </div>
    <div class="form-group">
        <label for="hide">hide</label>
        <select name="hide" id="hide">
            <option value="1">hide</option>
            <option value="0">view</option>
        </select>
    </div>
    <div class="form-group">
        <label for="category_id">category</label>
        <select name="category_id" class="form-control-select" id="category_id" >
            <option value="1">Man</option>
            <option value="2">Woman</option>
        </select>
        {{-- <input type="number" name="category_id" class="form-control-select" id="category_id" > --}}
    </div>
    <div class="form-group">
        <label for="weather_id">Weather</label>
        {{-- <select name="weather_id" class="form-control-select" id="weather_id" >
            <option value="1">Summer</option>
            <option value="2">Winter</option>
            <option value="3">Autumn</option>
            <option value="4">Spring</option>
        </select> --}}
        <input type="checkbox" name="weather_id[]" id="weather_id" value="1">summer
        <input type="checkbox" name="weather_id[]" id="weather_id" value="2">winter
        <input type="checkbox" name="weather_id[]" id="weather_id" value="3">autumn
        <input type="checkbox" name="weather_id[]" id="weather_id" value="4">spring
    </div>
    
    <button type="submit" class="btn btn-primary">insert</button>
</form>
@endsection