@extends('layouts.adminlayout')

@section('content')

<h1>Edit a new coupon</h1>
<form  method="post" action="{{route('admin.coupons.update',$coupon->id)}}" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" id="name" width="40%" value="{{$coupon->name}}" >
    </div>

    <div class="form-group">
        <label for="value">value</label>
        <input type="number" name="value" class="form-control-select" id="value" value="{{$coupon->value}}">
    </div>
        {{-- <input type="number" name="category_id" class="form-control-select" id="category_id" > --}}
        
    <button type="submit" class="btn btn-primary">update</button>
</form>
@endsection