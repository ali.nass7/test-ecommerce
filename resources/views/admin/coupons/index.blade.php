@extends('layouts.adminlayout')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
            <tr>
                <th>name</th>
                <th>value</th>
                <th>delete</th>
                <th>edit</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>name</th>
                <th>value</th>
                <th>delete</th>
                <th>edit</th>
            </tr>
          </tfoot>
          <tbody>
            @foreach ($coupons as $coupon )
            <tr>
                <td> {{$coupon->name}} </td>
                <td> {{$coupon->value}} </td>
                <td> 
                  <form method="POST" action="{{route('admin.coupons.destroy',$coupon->id)}}">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Delete</button>
                  </form>  
                </td>
                <td> 
                  <a href="{{route('admin.coupons.edit',$coupon->id)}}">Edit</a>
                </td>
            </tr>
            @endforeach
          </tbody>
      </div>
    </div>
  </div>


@endsection

@section('scripts')
      <!-- Page level plugins -->
  <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
    {{-- <script src="{{asset('js/datatabels')}}"></script> --}}
@endsection