@extends('layouts.adminlayout')

@section('content')

<h1>Insert a new Coupons</h1>
<form action="{{route('admin.coupons.store')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" id="name" width="40%" >
    </div>
    <div class="form-group">
        <label for="value">value</label>
        <input type="number" step=0.01 min="0" max="100" name="value" class="form-control" id="value" width="40%" >
    </div>
    
    
    <button type="submit" class="btn btn-primary">insert</button>
</form>
@endsection