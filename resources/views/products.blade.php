@extends('layouts.app')

@section('content')

<!-- Items Starts Here -->
<div class="featured-page">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-12">
            <div class="section-heading">
              <div class="line-dec"></div>
              <h1>Featured Items</h1>
            </div>
          </div>
          <div class="col-md-8 col-sm-12">
            <div id="filters" class="button-group">
              {{-- <a href="{{route('product')}}" class="btn btn-primary">All</a> --}}
              <a href="{{route('product.low')}}" class="btn btn-primary">Low</a>
              <a href="{{route('product.hight')}}" class="btn btn-primary">Hight</a>
              {{-- <button class="btn btn-primary" data-filter="*">All Products</button>
              <button class="btn btn-primary" data-filter=".new">Newest</button>
              <button class="btn btn-primary" data-filter=".low">Low Price</button>
              <button class="btn btn-primary" data-filter=".high">Hight Price</button> --}}
            </div>
          </div>
        </div>
      </div>
    </div>
  
    <div class="featured container no-gutter">

      <div class="row posts">
        @foreach ($products as $product )
        <div id="{{$product->id}}" class="item col-md-4">
              <a href="{{route('product.show',$product->id)}}" >
                <div class="featured-itemss">
                  <img src="{{asset($product->path)}}" alt="" >
                  <h4>{{$product->name}}</h4>
                  <h6>{{$product->price}} s.p</h6>
                </div>
              </a>
            </div>
            @endforeach
            
          </div>
        </div>
        {{-- <div class="page-navigation">
        <div class="d-flex">
          <div class="mx-auto">
            {{$products->links()}}
          </div>
        </div>
      </div> --}}
      
      {{-- <div class="page-navigation">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul>
                {{$products->links()}}

            </ul>
          </div>
          </div>
          </div>
          </div> --}}

      
    <div class="page-navigation">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <ul>
              <li class="current-page"><a href="#">1</a></li>
              {{-- <li><a href="#">2</a></li>
              <li><a href="#">3</a></li> --}}
              <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- Featred Page Ends Here -->


    <!-- Subscribe Form Starts Here -->
    <div class="subscribe-form">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="section-heading">
              <div class="line-dec"></div>
              <h1>Subscribe on PIXIE now!</h1>
            </div>
          </div>
          <div class="col-md-8 offset-md-2">
            <div class="main-content">
              <p>Godard four dollar toast prism, authentic heirloom raw denim messenger bag gochujang put a bird on it celiac readymade vice.</p>
              <div class="container">
                <form id="subscribe" action="" method="get">
                  <div class="row">
                    <div class="col-md-7">
                      <fieldset>
                        <input name="email" type="text" class="form-control" id="email" 
                        onfocus="if(this.value == 'Your Email...') { this.value = ''; }" 
                    	onBlur="if(this.value == '') { this.value = 'Your Email...';}"
                    	value="Your Email..." required="">
                      </fieldset>
                    </div>
                    <div class="col-md-5">
                      <fieldset>
                        <button type="submit" id="form-submit" class="button">Subscribe Now!</button>
                      </fieldset>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Subscribe Form Ends Here -->


    @endsection