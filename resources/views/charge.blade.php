@extends('layouts.app')

@section('content')

 
<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
  <div class="container">
    <a class="navbar-brand" href="#"><img src=" " alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item ">
          <a class="nav-link" href="{{route('order.index')}}">Orders
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{ route('charge.create')}}">Charge
          </a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="{{ route('charge.index')}}">Charges History
          </a>
        </li>
        </ul>
    </div>
  </div>
</nav>

<div class="featured-page">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="section-heading">
            {{-- <div class="line-dec"></div>
            <h1>Your Amount is: {{$money}} </h1>
            <h1>To Charge Your Account Send Money To This Num </h1>
            <h2>+963934912912 </h2> --}}
          </div>
        </div>
      </div>
      <div class="blue">
        <a href="{{route('order.index')}}">orders </a>
      </div>
      <div class="centered-box ">
        <form action="{{route('charge.store')}}" method="post">
          @csrf
          {{-- <label>Your Amount is: {{$money}} </label> --}}
          <h4>To Charge Your Account Send Money To This Num </h4>
          <h5 class="phonen">+962934912912 </h5>
          <div class="form-group">
                <label for="value">Value:</label>
                <input type="number" name="value" class="" id="value">
              </div>
            <div class="form-group">
                <label for="code">Code:</label>
                <input type="text" name="code" class="" id="code">
              </div>
              <input type="hidden" name="balance" value="{{auth()->user()->money}}" id="balance">
              <button type="submit" class="btn btn-primary centered-button">Charge</button>
          </form>
      </div>
    </div>
    </div>
        @endsection