<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use database\Seeders\CategoriesTableSeeder;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('weather')->insert([
            'id'=>1,
            'name'=>'summer',
            'activate'=>1
        ]);
        DB::table('weather')->insert([
            'id'=>2,
            'name'=>'winter',
            'activate'=>0
        ]);
        DB::table('weather')->insert([
            'id'=>3,
            'name'=>'autumn',
            'activate'=>0
        ]);
        DB::table('weather')->insert([
            'id'=>4,
            'name'=>'spring',
            'activate'=>0
        ]);

        DB::table('categories')->insert([
            'name' => 'man'
            ]);
        DB::table('categories')->insert([
            'name' => 'woman'
            ]);

        DB::table('coupons')->insert([
            'name' => 'tishreen',
            'value'=> 0.7
            ]);

        DB::table('users')->insert([
            'id'=> 1,
            'name' => 'admin',
            'is_admin' => true,
            'email'=>'admin@admin.com',
            'password'=>Hash::make('123')
            ]);

        DB::table('users')->insert([
            'id'=> 2,
            'name' => 'ali',
            'is_admin' => false,
            'email'=>'ali@gmail.com',
            'money'=>200000,
            'password'=>Hash::make('123')
            ]);

        // DB::table('product')
        
        // \App\Models\User::factory(10)->create();
        // Category::factory()->make(['name'=>'woman']);
        /**
 * Run the database seeders.
 */
// public function run(): void
// {
    // $this->call([
    //     CategoriesTableSeeder::class
    //     // PostSeeder::class,
    //     // CommentSeeder::class,
    // ]);
// }
    }
}
