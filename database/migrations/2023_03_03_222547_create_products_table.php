<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id()->nullable();
            $table->foreignId('user_id')->nullable();
            $table->string('name')->nullable();
            $table->text('details')->nullable();
            $table->double('purchase');
            $table->double('price')->nullable();
            $table->string('path')->nullable();
            $table->integer('q_large')->nullable();
            $table->integer('q_medium')->nullable();
            $table->integer('q_small')->nullable();
            $table->boolean('hide')->default(false);
            $table->foreignId('category_id')->nullable();
            $table->softDeletes();
            // $table->foreignId('weather_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
