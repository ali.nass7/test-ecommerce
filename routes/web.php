<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ChargeController;
use App\Http\Controllers\CouponsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\WeatherController;
use App\Models\Products;
use App\Models\Weather;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/sop',function(){
    return view('sspf');
});
Route::get('/w', function () {
    return view('welcome');
});
Route::get('/about', function () {
    return view('about');
})->name('about');

Auth::routes();


// Route::get('/single-product', function(){
//     return view('single-product');
// });

Route::get('/products',function(){
    // $products=Products::where('hide',0)->get();
    // return view('products',[
    //     'products'=>$products
    // ]);
    $products = Products::whereHas('weathers', function ($query) {
        $weather=Weather::where('activate',1)->first();
        $query->where('name',$weather->name)->where('hide',0)->orderBy('price','DESC');
    })->get();
    return view('products',[
        'products'=>$products
    ]);
});

// Route::get('products/',[App\Http\Controllers\ProductsController::class,'index'])->name('product');



Route::middleware(['IsAdmin','auth'])->group(function(){
    // Route::get('products/order/history',[OrderController::class,'adminIndex'])->name('admin.order.index');
    Route::get('/asd',function(){
        return "22";
    });

    Route::patch('/admin/weather/change',[WeatherController::class,'update'])->name('admin.weather.change');
    Route::get('admin/charge',[ChargeController::class,'indexAdmin'])->name('admin.charge.index');
    Route::get('admin/charge/history',[ChargeController::class,'history'])->name('admin.charge.history');
    Route::get('admin/charge/create',[ChargeController::class,'createCh'])->name('admin.charge.create');
    Route::post('admin/charge/store',[ChargeController::class,'storeCh'])->name('admin.charge.store');
    Route::patch('admin/charge/{charge}/update',[ChargeController::class,'update'])->name('admin.charge.update');
    Route::delete('admin/charge/{charge}/delete',[ChargeController::class,'destroy'])->name('admin.charge.destroy');
    Route::get('admin/products/order',[OrderController::class,'indexAdmin'])->name('admin.order.index');
    Route::get('admin/products/create',[ProductsController::class,'create'])->name('product.create');
    Route::get('admin/products',[ProductsController::class,'indexAdmin'])->name('admin.product.index');
    Route::post('admin/products/store',[ProductsController::class,'store'])->name('admin.product.store');
    Route::delete('admin/products/{product}/destroy',[ProductsController::class,'destroy'])->name('admin.product.destroy');
    Route::patch('admin/products/{product}/showp',[ProductsController::class,'showp'])->name('admin.product.showp');
    Route::patch('admin/products/{product}/hide',[ProductsController::class,'hide'])->name('admin.product.hide');
    Route::get('admin/products/{product}/edit',[ProductsController::class,'edit'])->name('admin.product.edit');
    Route::patch('admin/products/{product}/update',[ProductsController::class,'update'])->name('admin.product.update');
    Route::get('admin/coupons/create',[CouponsController::class,'create'])->name('coupons.create');
    Route::get('admin/coupons/{coupon}/edit',[CouponsController::class,'edit'])->name('admin.coupons.edit');
    Route::patch('admin/coupons/{coupon}/update',[CouponsController::class,'update'])->name('admin.coupons.update');
    Route::get('admin/coupons',[CouponsController::class,'index'])->name('admin.coupons.index');
    Route::post('admin/coupons/store',[CouponsController::class,'store'])->name('admin.coupons.store');
    Route::delete('admin/coupons/{coupon}/destroy',[CouponsController::class,'destroy'])->name('admin.coupons.destroy');
    Route::get('/admin',[AdminController::class,'index'])->name('admin.index');
    // Route::get('/', [HomeController::class, 'indexAdmin'])->name('homeAdmin');
});

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::middleware('auth')->group(function(){
    // Route::get('/asd',function(){
    //     return "77";
    // });
    // Route::get('/pp',[ProductsController::class,'ss'])->name('pp');
    Route::get('/charge/history',[ChargeController::class,'index'])->name('charge.index');
    Route::get('/charge/create',[ChargeController::class,'create'])->name('charge.create');
    Route::post('/charge/store',[ChargeController::class,'store'])->name('charge.store');
    Route::post('/products/order/store',[OrderController::class,'store'])->name('order.store');
    Route::get('order/history',[OrderController::class,'index'])->name('order.index');
    Route::get('/ourproducts',[ProductsController::class,'index'])->name('product');
    Route::get('products/{product}/show',[ProductsController::class,'show'])->name('product.show');
    Route::get('prouducts/coupons',[CouponsController::class,'check'])->name('coupon.check');
});
Route::get('/ourproducts/low',[ProductsController::class,'indexLow'])->name('product.low');
Route::get('/ourproducts/hight',[ProductsController::class,'indexHight'])->name('product.hight');

// Route::resource('/admin',AdminController::class);