<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $guarded=[];
    
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function weathers(){
        return $this->belongsToMany (Weather::class);
    }
    // public function weather(){
    //     return $this->belongsTo(Weather::class);
    // }

    public function order(){
        return $this->hasMany(Order::class);
    }
    
    public function getPathAttribute($value){
        return asset('storage/'.$value);
    }
}
