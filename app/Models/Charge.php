<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Charge extends Model
{
    // use SoftDeletes;
    // use HasFactory;
    protected $guarded=[];
    // public $dates=['deleted_at'];
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function status(){
        return $this->hasOne(Status::class);
    }
}
