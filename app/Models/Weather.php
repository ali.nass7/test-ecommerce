<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Weather extends Model
{
    use HasFactory;
    protected $fillable=['id','name','activate'];
    protected $primaryKey='id';

    // public function products(){
    //     return $this->hasMany(Products::class);
    // }
    // public function products(){
    //     return $this->belongsToMany(Products::class,'products_weather');
    // }
    public function products(){
        return $this->hasMany(Products::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
