<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Products;
use App\Models\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    public function indexAdmin(){
        $orders=Order::all();
        return view('admin.orders.index',['orders'=>$orders]);
    }

    public function index(){
        $user=User::findOrFail(auth()->user()->id);
        $orders=Order::where('user_id',$user->id)->paginate(10);
        // dd($orders);
        return view('orderHistory',['orders'=>$orders]);
    }

    public function store()
    {
        $inputs=request()->all();
        $inputs['price']=$inputs['price']* $inputs['quantity'];
        if($inputs['price'] > auth()->user()->money)
        return back();
        $product=Products::findOrFail($inputs['product_id']);
        
        
        if($inputs['size']=='large')
        {
            if($inputs['quantity']>$product->q_large)
            return back();
            $product->q_large=$product->q_large - $inputs['quantity'];
        }elseif($inputs['size']=='small'){
            if($inputs['quantity']>$product->q_small)
            return back();
            $product->q_small=$product->q_small - $inputs['quantity'];
        }else{
            if($inputs['quantity']>$product->q_medium)
            return back();
            $product->q_medium=$product->q_medium - $inputs['quantity'];
        }
        auth()->user()->orders()->create($inputs);
        $user=User::findOrFail(auth()->user()->id);
        // if($inputs['coupon'] =='no')
        $user->money= $user->money - ($inputs['price'] ) ;
        $user->discount=1;
        $product->save();
        $user->save();   
        // Session::flash('message-product-created','product has created');
        return back();
    }


}
