<?php

namespace App\Http\Controllers;
use App\Models\Charge;
use App\Models\User;
use Illuminate\Http\Request;

class ChargeController extends Controller
{
 
    public function create()
    {
        $money=auth()->user()->money;
        return view('charge',['money'=>$money]);
    }
    public function createCh()
    {
        return view('admin.charges.create');
    }
    public function storeCh()
    {
        $inputs=request()->all();
        $user=User::where('email',$inputs['email'])->first();
        // dd($user);
        $user->money=$user->money +$inputs['value'];
        $user->save();
        return back();
    }

    public function index(){
        $charges= Charge::where('user_id',auth()->user()->id)->get();
        return view('chargesHistory',['charges'=>$charges]);
    }

    public function indexAdmin(){
        $charges=Charge::where('status_id',0)->get();
        return view('admin.charges.index',['charges'=>$charges]);
    }
    public function history(){
        $charges=Charge::where('status_id',1)->get();
        return view('admin.charges.history',['charges'=>$charges]);
    }

    public function update(Charge $charge)
    {
        $user=User::findOrFail($charge->user_id);
        $user->money=$user->money+$charge->value;
        $user->save();
        $charge->status_id=1;
        $charge->save();
        return back();
    }

    public function store(){
        $inputs=request()->all();
        auth()->user()->charges()->create($inputs);
        return back();
    }

    public function destroy(Charge $charge){
        // $charge->delete();
        $charge->status_id=2;
        $charge->save();
        return back();
    }
}
