<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Weather;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if(auth()->check()){
        if(auth()->user()->is_admin==1)
            return  redirect()->route('admin.index');
        else {
        $products = Products::whereHas('weathers', function ($query) {
            $weather=Weather::where('activate',1)->first();
            $query->where('name',$weather->name)->where('hide',0)->orderBy('price');
        })->get();
        return view('welcome',['products'=>$products]);}
    }else {
            $products = Products::whereHas('weathers', function ($query) {
                $weather=Weather::where('activate',1)->first();
                $query->where('name',$weather->name)->where('hide',0)->orderBy('price');
            })->get();
            return view('welcome',['products'=>$products]);}
    }
}
