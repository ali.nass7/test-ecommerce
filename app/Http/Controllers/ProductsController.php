<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProductsRequest;
use App\Http\Requests\UpdateProductsRequest;
use App\Models\Category;
use App\Models\Weather;
use Illuminate\Support\Facades\Session;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function ss(){
        $weather=Weather::where('activate',1)->first();
        $products=Products::where('name','fdsfsf')->first();
        return view('pp',['products'=>$products]);
     }

    public function index()
    {
        $products = Products::whereHas('weathers', function ($query) {
            $weather=Weather::where('activate',1)->first();
            $query->where('name',$weather->name)->where('hide',0);
        })->get();
        // })->paginate(2);
        // dd($products);
           return view('products',['products'=>$products]);
        }
        
    public function indexLow(){
        $products = Products::whereHas('weathers', function ($query) {
            $weather=Weather::where('activate',1)->first();
            $query->where('name',$weather->name)->where('hide',0);
        })->orderBy('price')->get();
        
        // $products=Products::where('weather_id',$weather->id)->where('hide',0)->orderBy('price')->get();
        // if(auth()->user()->id==1)
        // {
            //     return view('admin.products.index',['products'=>$products]);
        // }
        // else{
            //     return view('products',['products'=>$products]);
            // }
            return view('products',['products'=>$products]);
    }

    public function indexHight(){
        // $products = Products::whereHas('weathers', function ($query) {
        //     $weather=Weather::where('activate',1)->first();
        //     $query->where('name',$weather->name)->where('hide',0)->orderBy('price','DESC');
        // })->get();
        $products = Products::whereHas('weathers', function ($query) {
            $weather = Weather::where('activate', 1)->first();
            $query->where('name', $weather->name)->where('hide', 0);
        })->orderBy('price', 'DESC')->get();
            return view('products',['products'=>$products]);
    }

    public function indexAdmin(){
            $products=Products::with('weathers')->get();
            // dd($products);
            return view('admin.products.index',['products'=>$products]);
    }
 
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProductsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        // dd(request()->all());
        $inputs=request()->all();
        // $product= new Products;
        $product['name']=$inputs['name'];
        $product['details']=$inputs['details'];
        $product['purchase']=$inputs['purchase'];
        // dd($product);
        $product['q_large']=$inputs['q_large'];
        $product['q_medium']=$inputs['q_medium'];
        $product['q_small']=$inputs['q_small'];
        $product['hide']=$inputs['hide'];
        $product['category_id']=$inputs['category_id'];
        if(request('path')){
            $inputs['path']=request('path')->store('images');
        }
        $product['path']=$inputs['path'];
        $weather = $inputs['weather_id']; 
        $pro=auth()->user()->products()->create($product);
        $pro->weathers()->attach($weather);
        // dd($inputs);
        // dd($inputs);
        // $weather=Weather::find($inputs['weather_id']);
        // $product=auth()->user()->products()->create($inputs);
        // $product->weathers()->attach($weather);
        Session::flash('message-product-created','product has created');
        return redirect()->route('admin.product.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(Products $product)
    {
        // return dd();
        return view('single-product',['product'=>$product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $product)
    {
        return view('admin.products.edit',['product'=>$product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProductsRequest  $request
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Products $product)
    {
        $inputs=request()->all();
        // dd($inputs);
        $weather_ids = $inputs['weather_id'];
    $product->weathers()->sync($weather_ids);
        if(request('path'))
        {
            $inputs['path']=request('path')->store('images');
            $product->path=$inputs['path'];
        }
        $product->name=$inputs['name'];
        $product->details=$inputs['details'];
        $product->price=$inputs['price'];
        $product->q_large=$inputs['q_large'];
        $product->q_medium=$inputs['q_medium'];
        $product->q_small=$inputs['q_small'];
        $product->hide=$inputs['hide'];
        $product->category_id=$inputs['category_id'];
        
        auth()->user()->products()->save($product);
        
        return redirect()->route('admin.product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $product){
        $product->delete();
        Session::flash('message','product was deleted');
        return back();
    }
    
    public function hide(Products $product){
        $input=request()->all();
        auth()->user()->products()->update(['hide'=>'1']);
        return back();
    }
    public function showp(Products $product){
        $input=request()->all();
        auth()->user()->products()->update(['hide'=>'0']);
        return back();}
}
