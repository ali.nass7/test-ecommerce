<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Http\Requests\StoreAdminRequest;
use App\Http\Requests\UpdateAdminRequest;
use App\Models\Charge;
use App\Models\Order;
use App\Models\User;
use App\Models\Weather;

class AdminController extends Controller
{
     
    public function index()
    {
        $charges= Charge::all();
        // $usersMoney= $charges->where('state_id',1)->sum('value');
        $usersMoney= User::all()->sum('money');
        $countOfCharge= Charge::where('status_id',1)->count();
        $weathers=Weather::orderByRaw('activate DESC')->get();
        $orders= Order::all();
        $totalOrdars= $orders->count();
        $price= $orders->sum('price');
        $profit= $orders->sum('profit');
        $money=$price-$profit;
        $quantity=$orders->sum('quantity');
        $users=User::all()->count()-1;
        // dd($weathers);
        return view('admin.index',['weathers'=>$weathers,'quantity'=>$quantity,'money'=>$money,'totalOrdars'=>$totalOrdars,'usersMoney'=>$usersMoney,'countOfCharge'=>$countOfCharge,'users'=>$users]);
        // return view('admin.index',['weathers'=>$weathers]);
    }

     
    public function create()
    {
        //
    }
 
    public function store(StoreAdminRequest $request)
    {
        //
    }

     
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAdminRequest  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdminRequest $request, Admin $admin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }
}
