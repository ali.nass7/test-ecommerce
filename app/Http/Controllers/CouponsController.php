<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Coupons;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProductsRequest;
use App\Http\Requests\UpdateProductsRequest;
use App\Models\Category;
use Illuminate\Support\Facades\Session;
use Symfony\Component\Console\Input\Input;

class CouponsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $coupons=Coupons::all();
        return view('admin.coupons.index',['coupons'=>$coupons]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProductsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        $inputs=request()->all();
        // $new_value=1-$inputs['value'];
        $inputs['value']=1-$inputs['value'];
        auth()->user()->coupons()->create($inputs);
        // Session::flash('message-product-created','product has created');
        return redirect()->route('admin.coupons.index');
    }

    public function show(Coupons $coupons)
    {
        //
    }

    public function edit(Coupons $coupon)
    {
        return view('admin.coupons.edit',['coupon'=>$coupon]);
    }

    public function update(Coupons $coupon)
    {
        $inputs=request()->all();
        // dd($inputs);

        $coupon->name=$inputs['name'];
        $new_value=1-$inputs['value'];
        $coupon->value=$new_value;
        
        auth()->user()->coupons()->save($coupon);
        
        return redirect()->route('admin.coupons.index');
    }

    public function check(){
        $inputs=request()->all();
        $coupon_name=$inputs['coupon_name'];
        $coupon_value=Coupons::where('name',$coupon_name)->first();
        $res=$coupon_value->value;
        $user=User::findOrFail($inputs['user_id']);
        $user->discount=$res;
        // dd($user);
        $user->save();
        // $product_id=$inputs['product_id'];
        // $product=Products::findOrFail($product_id);
        // $product->discount=$res;
        // $product->save();
        return back();
    }
    public function destroy(Coupons $coupon){
        $coupon->delete();
        // Session::flash('message','product was deleted');
        return back();
    }
    
}
