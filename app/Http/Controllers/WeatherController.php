<?php

namespace App\Http\Controllers;

use App\Models\Weather;
use Illuminate\Http\Request;

class WeatherController extends Controller
{
    public function update()
    {
        $inputs=request()->all();
        // dd($inputs['weather']);
        // $weather
        // $weather=Weather::where('activate',1)->first();
        $weather=Weather::where('activate',1)->update(['activate'=>0]);
        // $weather->activate=0;
        // $weather->update(['activate'=>0]);
        // Weather::save($weather);

        $weather=Weather::findOrfail($inputs['weather'])->update(['activate'=>1]);
        // $weather->activate=1;
        // $weather->save();
        // dd($weather->name);
        return back();
    }
}
