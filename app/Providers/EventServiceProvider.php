<?php

namespace App\Providers;

use App\Models\Order;
use App\Observers\ProductObserver;
use App\Models\Products;
use App\Observers\OrderObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];
    // App\Models\Tour::create(['travel_id'=>996f6a0a-a7fd-40f9-bb4b-005da6984cfa,'name'=>'sss','starting_date'=>now(),'ending_date'=>now()->addDays(2),'price'=>100]);
    protected $observers=[Products::class=> [ProductObserver::class]];
    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
       Products::observe(ProductObserver::class);
       Order::observe(OrderObserver::class);
    }
}
